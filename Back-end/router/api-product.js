const express = require("express");
const router = express.Router();
module.exports = router;
const mysql = require("mysql");

const db_mysql = mysql.createConnection({
  user: "admin",
  host: "localhost",
  password: "Password",
  database: "paiduay_store_db",
  timeout: 100000,
});

//_________________________________________________________________________________________________________________________ Documents

// Add propduct
router.post("/addProduct", (req, res) => {
  const ID = "P" + String(Date.now());
  let name = req.body.name;
  let detail = req.body.detail;
  let price = req.body.price;
  let unit = req.body.unit;
  let sid = req.body.sid;
  let pcid = req.body.pcid;

  db_mysql.query(
    "INSERT INTO product_info (p_id,p_name,p_detail,p_price,p_unit,store_id,pc_id) VALUES(?,?,?,?,?,?,?)",
    [ID, name, detail, price, unit, sid, pcid],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// Edit propduct
router.put("/editProduct", (req, res) => {
  const ID = req.body.id;
  let name = req.body.name;
  let detail = req.body.detail;
  let price = req.body.price;
  let unit = req.body.unit;
  let sid = req.body.sid;
  let pcid = req.body.pcid;
  db_mysql.query(
    "UPDATE product_info " +
      "SET p_name = '" +
      name +
      "' , p_detail = '" +
      detail +
      "' , p_price = '" +
      price +
      "' , p_unit = '" +
      unit +
      "' , store_id = '" +
      sid +
      "' , pc_id = '" +
      pcid +
      "' WHERE p_id = '" +
      ID +
      "'",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// Delete product
router.delete("/deleteProduct/:id", (req, res) => {
  const ID = req.params.id;
  db_mysql.query(
    "DELETE FROM product_info WHERE p_id = '" + ID + "'",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// Propduct list all
router.get("/productList", (req, res) => {
  db_mysql.query(
    "SELECT * FROM product_info p " +
      "LEFT JOIN store_info s ON p.store_id = s.store_id " +
      "LEFT JOIN product_category pc ON p.pc_id = pc.pc_id ORDER BY p.p_id DESC",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// Propduct list by store
router.get("/productList/:id", (req, res) => {
  var id = req.params.id;
  db_mysql.query(
    "SELECT * FROM product_info p " +
      "LEFT JOIN store_info s ON p.store_id = s.store_id " +
      "LEFT JOIN product_category pc ON p.pc_id = pc.pc_id WHERE s.store_id = '" +
      id +
      "' ORDER BY p.p_id DESC",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// Propduct Category list all
router.get("/productCategoryList", (req, res) => {
  db_mysql.query("SELECT * FROM product_category", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
});

// Store name list all
router.get("/storeNameList", (req, res) => {
  db_mysql.query(
    "SELECT store_id , store_name FROM store_info order by store_id desc",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

//_________________________________________________________________________________________________________________________
// now use `db` to make your queries
