const express = require("express");
const router = express.Router();
module.exports = router;
const mysql = require("mysql");

const db_mysql = mysql.createConnection({
  user: "admin",
  host: "localhost",
  password: "password",
  database: "paiduay_store_db",
  timeout: 100000,
});

//_________________________________________________________________________________________________________________________ Documents

// Add store
router.post("/addStore", (req, res) => {
  const ID = "S" + String(Date.now());
  let name = req.body.name;
  let des = req.body.des;
  let tel = req.body.tel;
  let address = req.body.address;
  db_mysql.query(
    "INSERT INTO store_info (store_id,store_name,store_description,store_tel,store_address) VALUES(?,?,?,?,?)",
    [ID, name, des, tel, address],
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// Edit store
router.put("/editStore", (req, res) => {
  const ID = req.body.id;
  let name = req.body.name;
  let des = req.body.des;
  let tel = req.body.tel;
  let address = req.body.address;
  console.log(ID);
  db_mysql.query(
    "UPDATE store_info " +
      "SET store_name = '" +
      name +
      "' , store_description = '" +
      des +
      "' , store_tel = '" +
      tel +
      "' , store_address = '" +
      address +
      "' WHERE store_id = '" +
      ID +
      "'",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

// Store list
router.get("/storeList", (req, res) => {
  db_mysql.query(
    "SELECT * FROM store_info ORDER BY store_id DESC",
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    }
  );
});

//_________________________________________________________________________________________________________________________
// now use `db` to make your queries
