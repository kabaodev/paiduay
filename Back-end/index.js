const express = require("express");
const app = express();
const cors = require("cors");

app.use(cors());
app.use(express.json());

// Store API
const api_store = require("./router/api-store");
const api_product = require("./router/api-product");

app.use("/", api_store);
app.use("/", api_product);

app.listen("3100", () => {
  console.log("Server is running on port 3100");
});
