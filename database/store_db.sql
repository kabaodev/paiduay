CREATE DATABASE  IF NOT EXISTS `paiduay_store_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `paiduay_store_db`;
-- MySQL dump 10.13  Distrib 8.0.26, for macos11 (x86_64)
--
-- Host: localhost    Database: store_db
-- ------------------------------------------------------
-- Server version	5.7.36-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_category` (
  `pc_id` varchar(50) NOT NULL,
  `pc_name` varchar(50) NOT NULL,
  `pc_description` text NOT NULL,
  PRIMARY KEY (`pc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
INSERT INTO `product_category` VALUES ('pc20220126100000','ขนมขบเคี้ยว','ช่วยดับกระหาย เพิ่มความสดชื่น'),('pc20220126100001','นม','ช่วยดับกระหาย เพิ่มความสดชื่น'),('pc20220126100002','เครื่องดื่มอัดลมและน้ำหวาน','ช่วยดับกระหาย เพิ่มความสดชื่น'),('pc20220126100003','อื่นๆ','ช่วยดับกระหาย เพิ่มความสดชื่น');
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_info`
--

DROP TABLE IF EXISTS `product_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_info` (
  `p_id` varchar(50) NOT NULL,
  `p_name` varchar(50) NOT NULL,
  `p_detail` varchar(50) NOT NULL,
  `p_price` float NOT NULL,
  `p_unit` varchar(50) NOT NULL,
  `store_id` varchar(45) NOT NULL,
  `pc_id` varchar(45) NOT NULL,
  PRIMARY KEY (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_info`
--

LOCK TABLES `product_info` WRITE;
/*!40000 ALTER TABLE `product_info` DISABLE KEYS */;
INSERT INTO `product_info` VALUES ('P1643348641086','ข้าวไข่ดาว','อาหารจานด่วน',124,'จาน','S1643300800925','pc20220126100003'),('pc20220126100002','โค้ก','สูตรน้ำตาลน้อยกว่า',25,'ขวด','S1643300723243','pc20220126100002'),('pc20220126100004','โค้ก','สูตรน้ำตาลน้อยกว่า',25,'ขวด','S1643300833176','pc20220126100002'),('pc20220126100006','โค้ก','สูตรน้ำตาลน้อยกว่า',25,'ขวด','S1643300833176','pc20220126100002'),('pc20220126100007','โค้ก','สูตรน้ำตาลน้อยกว่า',25,'ขวด','S1643300833180','pc20220126100002'),('pc20220126100008','โค้ก','สูตรน้ำตาลน้อยกว่า',25,'ขวด','S1643300833180','pc20220126100002'),('pc20220126100010','โค้ก','สูตรน้ำตาลน้อยกว่า',3899,'ขวด','S1643300833176','pc20220126100002');
/*!40000 ALTER TABLE `product_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `store_info`
--

DROP TABLE IF EXISTS `store_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `store_info` (
  `store_id` varchar(50) NOT NULL,
  `store_name` varchar(50) NOT NULL,
  `store_description` text NOT NULL,
  `store_tel` int(10) NOT NULL,
  `store_address` text NOT NULL,
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `store_info`
--

LOCK TABLES `store_info` WRITE;
/*!40000 ALTER TABLE `store_info` DISABLE KEYS */;
INSERT INTO `store_info` VALUES ('S1643300723243','นมสด','นมเเท้จากเต้า',985372355,'-'),('S1643300800925','ข้าว','ขายข้าว',23523523,'12312 2/24'),('S1643300833176','เจ้นวล','ขายทุกอย่าง',82948567,'112 หมู่ 4 ต.แม่เหียะ อ.เมือง จ.เชียงใหม่ 50100'),('S1643300833180','คึกคัก','ขายตลอด 24 ชม',888888888,'155 หมู่ 2 ต.แม่เหียะ อ.เมือง จ.เชียงใหม่ 50100');
/*!40000 ALTER TABLE `store_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-28 12:58:59
