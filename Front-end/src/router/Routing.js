import React from "react";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";

//Page import
import EditStore from "../pages/Store/EditStore";
import AddProduct from "../pages/Product/AddProduct";
import EditProduct from "../pages/Product/EditProduct";
import Main from "../pages/main";

export default function Routing() {
  return (
    <Routes>
      <Route path="/" element={<Main />} />
      <Route path="/EditStore" element={<EditStore />} />
      <Route path="/AddProduct" element={<AddProduct />} />
      <Route path="/EditProduct" element={<EditProduct />} />
    </Routes>
  );
}
