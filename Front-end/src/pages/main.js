import React from "react";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";
import {
  SimpleGrid,
  Box,
  Grid,
  GridItem,
  Text,
  Stack,
  IconButton,
  useDisclosure,
} from "@chakra-ui/react";
import { EditIcon, AddIcon, DeleteIcon } from "@chakra-ui/icons";
import axios from "axios";

import Modal from "./Store/AddStore";

const Main = () => {
  const [storeList, setStoreList] = React.useState([]);
  const [productList, setProductList] = React.useState([]);
  const [dltStatus, setDltStatus] = React.useState(false);

  const { isOpen, onOpen, onClose } = useDisclosure();

  React.useEffect(() => {
    getStoreList();
    getProductList();
  }, [isOpen]);

  React.useEffect(() => {
    getProductList();
  }, [dltStatus]);

  const getStoreList = () => {
    axios.get("http://localhost:3100/storeList").then((response) => {
      let data = response.data;
      setStoreList(data);
    });
  };

  const getProductList = () => {
    axios.get("http://localhost:3100/productList").then((response) => {
      let data = response.data;
      setProductList(data);
    });
  };

  const getProductListById = (id) => {
    axios.get("http://localhost:3100/productList/" + id).then((response) => {
      let data = response.data;
      setProductList(data);
    });
  };

  const deleteProduct = (id) => {
    axios
      .delete("http://localhost:3100/deleteProduct/" + id)
      .then((response) => {
        let data = response.data;
        setDltStatus(!dltStatus);
      });
  };

  const StoreList = () => {
    return storeList.map((res) => {
      var link =
        "/EditStore?id=" +
        res.store_id +
        "&name=" +
        res.store_name +
        "&des=" +
        res.store_description +
        "&tel=" +
        res.store_tel +
        "&address=" +
        res.store_address;
      return (
        <Box
          p={5}
          shadow="md"
          borderRadius="15px"
          minW={{ base: "200px", xl: "100%" }}
          onClick={() => {
            getProductListById(res.store_id);
          }}
          cursor={'pointer'}
        >
          <Box
            display={"flex"}
            justifyContent={"space-between"}
            alignItems={"center"}
          >
            <Text
              textAlign={"start"}
              fontWeight={"bold"}
              fontSize={{ base: "1rem", xl: "1.5rem" }}
            >
              {res.store_name}
            </Text>
            <Link to={link}>
              <IconButton
                colorScheme="yellow"
                icon={<EditIcon />}
                size={"xs"}
              />
            </Link>
          </Box>

          <Text textAlign={"start"} fontSize={{ base: "0.5rem", xl: "1rem" }}>
            {res.store_description}
          </Text>
          <Text textAlign={"start"} fontSize={{ base: "0.5rem", xl: "1rem" }}>
            {res.store_tel}
          </Text>
          <Text textAlign={"start"} fontSize={{ base: "0.5rem", xl: "1rem" }}>
            {res.store_address}
          </Text>
        </Box>
      );
    });
  };

  const ProductList = () => {
    return productList.map((res) => {
      var link =
        "/EditProduct?id=" +
        res.p_id +
        "&name=" +
        res.p_name +
        "&detail=" +
        res.p_detail +
        "&price=" +
        res.p_price +
        "&unit=" +
        res.p_unit +
        "&sid=" +
        res.store_id +
        "&pcid=" +
        res.pc_id +
        "&sname=" +
        res.store_name +
        "&pcname=" +
        res.pc_name;
      return (
        <GridItem>
          <Box
            p={5}
            shadow="md"
            borderRadius="15px"
            w={{ base: "100%", xl: "100%" }}
            minH="100%"
          >
            <Text
              textAlign={"center"}
              fontSize={{ base: "1rem", md: "0.7rem", xl: "1rem" }}
              opacity={0.5}
            >
              {res.store_name}
            </Text>
            <Text
              textAlign={"start"}
              fontWeight={"bold"}
              fontSize={{ base: "1.5rem", xl: "1.2rem" }}
            >
              {res.p_name}
            </Text>
            <Text
              textAlign={"start"}
              fontSize={{ base: "1rem", md: "0.7rem", xl: "1rem" }}
            >
              {res.p_detail}
            </Text>
            <Text
              textAlign={"start"}
              fontSize={{ base: "1rem", md: "0.7rem", xl: "1rem" }}
            >
              ราคา : {res.p_price} บาท
            </Text>
            <Text
              textAlign={"start"}
              fontSize={{ base: "1rem", md: "0.7rem", xl: "1rem" }}
            >
              หน่วย : {res.p_unit}
            </Text>
            <Text
              textAlign={"start"}
              fontSize={{ base: "1rem", md: "0.7rem", xl: "1rem" }}
            >
              หมวดหมู่ : {res.pc_name}
            </Text>
            <Box
              display={"flex"}
              justifyContent={"space-between"}
              alignItems={"center"}
              mt="0.5rem"
            >
              <Link to={link}>
                <IconButton
                  colorScheme="yellow"
                  icon={<EditIcon />}
                  size={"xs"}
                />
              </Link>
              <IconButton
                colorScheme="red"
                icon={<DeleteIcon />}
                size={"xs"}
                onClick={() => {
                  deleteProduct(res.p_id);
                }}
              />
            </Box>
          </Box>
        </GridItem>
      );
    });
  };

  return (
    <Grid h="100%" templateColumns="repeat(12, 1fr)" gap={4}>
      <GridItem colSpan={12} bg="tomato" py="1rem">
        <Text fontSize={"2rem"} fontWeight={"bold"}>
          ระบบจัดการร้านค้า
        </Text>
      </GridItem>
      <GridItem
        colSpan={{ base: 12, xl: 4 }}
        border="1px solid"
        borderColor="gray.200"
        borderRadius="15px"
        ml={{ base: "2vw", xl: "10vw" }}
        mr={{ base: "2vw", xl: 0 }}
      >
        <Box
          display={"flex"}
          p="1rem 1rem 0 1rem"
          justifyContent={"space-between"}
          alignItems={"center"}
        >
          <Text
            textAlign={"start"}
            fontSize={{ base: "1rem", md: "0.9rem", xl: "1.3rem" }}
            fontWeight={"bold"}
          >
            ร้านค้า
          </Text>
          <IconButton
            colorScheme="green"
            icon={<AddIcon />}
            size={"sm"}
            onClick={onOpen}
          />
        </Box>

        <Stack
          direction={{ base: "row", xl: "column" }}
          spacing="24px"
          p={"1rem"}
          overflow="auto"
        >
          {StoreList()}
        </Stack>
      </GridItem>
      <GridItem
        colSpan={{ base: 12, xl: 8 }}
        mr={{ base: "2vw", xl: "10vw" }}
        ml={{ base: "2vw", xl: 0 }}
      >
        <Stack direction={{ base: "column", xl: "column" }} spacing="24px">
          <Box
            display={"flex"}
            justifyContent={"center"}
            alignItems={"center"}
            bg="gray.200"
            py="0.5rem"
            borderRadius={"15px 15px 0 0"}
          >
            <Text
              textAlign={"start"}
              fontSize={{ base: "1rem", md: "0.9rem", xl: "1.3rem" }}
              fontWeight={"bold"}
              mr="1rem"
            >
              สินค้า
            </Text>
            <Link to={"/AddProduct"}>
              <IconButton
                colorScheme="green"
                icon={<AddIcon />}
                size={"sm"}
                onClick={onOpen}
              />
            </Link>
          </Box>
          <Grid
            templateColumns={{
              base: "repeat(1, 1fr)",
              sm: "repeat(2, 1fr)",
              md: "repeat(4, 1fr)",
              xl: "repeat(3, 1fr)",
              "2xl": "repeat(4, 1fr)",
            }}
            gap={{ base: 6, xl: 3 }}
          >
            {ProductList()}
          </Grid>
        </Stack>
      </GridItem>
      <Modal isOpen={isOpen} onClose={onClose} />
    </Grid>
  );
};

export default Main;
