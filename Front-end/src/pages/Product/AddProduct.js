import React from "react";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";
import {
  Box,
  FormControl,
  FormLabel,
  Input,
  Button,
  Grid,
  GridItem,
  Text,
  Select,
} from "@chakra-ui/react";
import axios from "axios";

function getParam(param) {
  return new URLSearchParams(window.location.search).get(param);
}

export default function AddProduct() {
  const [name, setName] = React.useState();
  const [detail, setDetail] = React.useState();
  const [price, setPrice] = React.useState();
  const [unit, setUnit] = React.useState();

  const [sid, setSid] = React.useState();
  const [pcid, setPcid] = React.useState();

  const [storeList, setStoreList] = React.useState([]);
  const [categoryList, setCategoryList] = React.useState([]);

  React.useEffect(() => {
    getStoreName();
    getCategory();
  }, []);

  const getStoreName = () => {
    axios.get("http://localhost:3100/storeNameList").then((response) => {
      let data = response.data;
      setStoreList(data);
    });
  };

  const getCategory = () => {
    axios.get("http://localhost:3100/productCategoryList").then((response) => {
      let data = response.data;
      setCategoryList(data);
    });
  };

  const submit = () => {
    axios
      .post("http://localhost:3100/addProduct", {
        name: name,
        detail: detail,
        price: price,
        sid: sid,
        unit: unit,
        pcid: pcid,
      })
      .then((response) => {
        setName("");
        setDetail("");
        setPrice(0);
        setUnit("");
        sid("");
        pcid("");
      });
  };

  const slcStore = () => {
    return storeList.map((res) => {
      return <option value={res.store_id}>{res.store_name}</option>;
    });
  };

  const slcCategory = () => {
    return categoryList.map((res) => {
      return <option value={res.pc_id}>{res.pc_name}</option>;
    });
  };

  return (
    <Grid h="100%" templateColumns="repeat(12, 1fr)" gap={4}>
      <GridItem colSpan={12} bg="blue.300" py="1rem">
        <Text fontSize={"2rem"} fontWeight={"bold"}>
          เพิ่มสินค้า
        </Text>
      </GridItem>
      <GridItem
        colSpan={{ base: 12, xl: 12 }}
        border="1px solid"
        borderColor="gray.200"
        borderRadius="15px"
        ml={{ base: "2vw", xl: "10vw" }}
        mr={{ base: "2vw", xl: "10vw" }}
        p={"2rem"}
      >
        <Box>
          <FormControl>
            <FormLabel>ชื่อร้าน</FormLabel>
            <Select
              placeholder="ชื่อร้าน"
              onChange={(e) => {
                setSid(e.target.value);
              }}
            >
              {slcStore()}
            </Select>
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>ชื่อสินค้า</FormLabel>
            <Input
              placeholder="ชื่อสินค้า"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>หมวดหมู่</FormLabel>
            <Select
              placeholder="หมวดหมู่"
              onChange={(e) => {
                setPcid(e.target.value);
              }}
            >
              {slcCategory()}
            </Select>
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>รายละเอียด</FormLabel>
            <Input
              placeholder="รายละเอียด"
              value={detail}
              onChange={(e) => {
                setDetail(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>ราคา</FormLabel>
            <Input
              placeholder="ราคา"
              type={"number"}
              value={price}
              onChange={(e) => {
                setPrice(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>หน่วย</FormLabel>
            <Input
              placeholder="หน่วย"
              value={unit}
              onChange={(e) => {
                setUnit(e.target.value);
              }}
            />
          </FormControl>
          <Button colorScheme="blue" mr={3} mt={3} onClick={submit}>
            เพิ่ม
          </Button>
          <Link to="/">
            <Button mt={3}>ยกเลิก</Button>{" "}
          </Link>
        </Box>
      </GridItem>
    </Grid>
  );
}
