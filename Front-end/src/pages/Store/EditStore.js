import React from "react";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";
import {
  Box,
  FormControl,
  FormLabel,
  Input,
  Button,
  Grid,
  GridItem,
  Text,
} from "@chakra-ui/react";
import axios from "axios";

function getParam(param) {
  return new URLSearchParams(window.location.search).get(param);
}

export default function AddStore() {
  const [id, setId] = React.useState();
  const [name, setName] = React.useState();
  const [des, setDes] = React.useState();
  const [tel, setTel] = React.useState();
  const [address, setAddress] = React.useState();

  React.useEffect(() => {
    setId(getParam("id"));
    setName(getParam("name"));
    setDes(getParam("des"));
    setTel(getParam("tel"));
    setAddress(getParam("address"));
  }, []);

  const submit = () => {
    axios
      .put("http://localhost:3100/editStore", {
        id: id,
        name: name,
        des: des,
        tel: tel,
        address: address,
      })
      .then((response) => {
        let data = response;
        console.log(data);
      });
  };

  return (
    <Grid h="100%" templateColumns="repeat(12, 1fr)" gap={4}>
      <GridItem colSpan={12} bg="yellow.300" py="1rem">
        <Text fontSize={"2rem"} fontWeight={"bold"}>
          เเก้ไขร้านค้า
        </Text>
      </GridItem>
      <GridItem
        colSpan={{ base: 12, xl: 12 }}
        border="1px solid"
        borderColor="gray.200"
        borderRadius="15px"
        ml={{ base: "2vw", xl: "10vw" }}
        mr={{ base: "2vw", xl: "10vw" }}
        p={"2rem"}
      >
        <Box>
          <FormControl>
            <FormLabel>ชื่อร้าน</FormLabel>
            <Input
              placeholder="ชื่อร้าน"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>คำอธิบาย</FormLabel>
            <Input
              placeholder="คำอธิบาย"
              value={des}
              onChange={(e) => {
                setDes(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>เบอร์โทร</FormLabel>
            <Input
              placeholder="เบอร์โทร"
              value={tel}
              onChange={(e) => {
                setTel(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>ที่อยู่</FormLabel>
            <Input
              placeholder="ที่อยู่"
              value={address}
              onChange={(e) => {
                setAddress(e.target.value);
              }}
            />
          </FormControl>
          <Button colorScheme="blue" mr={3} mt={3} onClick={submit}>
            เเก้ไข
          </Button>
          <Link to="/">
            <Button mt={3}>ยกเลิก</Button>{" "}
          </Link>
        </Box>
      </GridItem>
    </Grid>
  );
}
