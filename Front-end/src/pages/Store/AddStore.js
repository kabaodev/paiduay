import React from "react";
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  FormControl,
  FormLabel,
  Input,
  Button,
} from "@chakra-ui/react";
import axios from "axios";

export default function AddStore(props) {
  const [name, setName] = React.useState();
  const [des, setDes] = React.useState();
  const [tel, setTel] = React.useState();
  const [address, setAddress] = React.useState();

  React.useEffect(() => {}, []);

  const submit = () => {
    axios
      .post("http://localhost:3100/addStore", {
        name: name,
        des: des,
        tel: tel,
        address: address,
      })
      .then((response) => {
        let data = response.data;
        console.log(data);
        props.onClose();
      });
  };

  return (
    <Modal isOpen={props.isOpen} onClose={props.onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>เพิ่มร้านค้า</ModalHeader>
        <ModalCloseButton />
        <ModalBody pb={6}>
          <FormControl>
            <FormLabel>ชื่อร้าน</FormLabel>
            <Input
              placeholder="ชื่อร้าน"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>คำอธิบาย</FormLabel>
            <Input
              placeholder="คำอธิบาย"
              value={des}
              onChange={(e) => {
                setDes(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>เบอร์โทร</FormLabel>
            <Input
              placeholder="เบอร์โทร"
              value={tel}
              onChange={(e) => {
                setTel(e.target.value);
              }}
            />
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>ที่อยู่</FormLabel>
            <Input
              placeholder="ที่อยู่"
              value={address}
              onChange={(e) => {
                setAddress(e.target.value);
              }}
            />
          </FormControl>
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={submit}>
            เพิ่ม
          </Button>
          <Button onClick={props.onClose}>ยกเลิก</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
