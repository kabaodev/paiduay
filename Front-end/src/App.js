import "./App.css";
import Main from "./pages/main";
import Routing from "./router/Routing";
import { ChakraProvider } from "@chakra-ui/react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <ChakraProvider>
        <Router>
          <Routing />
        </Router>
      </ChakraProvider>
    </div>
  );
}

export default App;
